# Flexslider SCSS

Flexslider styles in SCSS format to quickly import.

Using
-----
First you need to tell SASS where your flexslider installation is.
To do that set the variable `$flexslider-path` to the correct path.

Then import your script like any other, using `@import`.

Installing with bower:
----------------------

Run `bower i --save flexslider-scss=git@bitbucket.org:Battousai/flexslider-scss.git`
in your terminal.
